import React from 'react'
import { Nav } from '../../common/Nav';
import { Footer } from '../../common/Footer';

import { Container, Grid,Stack,Card,CardActions,CardContent,Box,Typography, createTheme,Button,ThemeProvider} from '@mui/material';
import BusinessCenterIcon from '@mui/icons-material/BusinessCenter';

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme();

export const HomePage: React.FC<{}> = () => {
  return (
    <Box>
      <Nav />
      <ThemeProvider theme={defaultTheme}>
      
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: 'background.paper',
            pt: 4,
            pb: 6,
          }}
        >
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              Bolsa de empleos activa
            </Typography>
            
            <Stack
              sx={{ pt: 4 }}
              direction="row"
              spacing={2}
              justifyContent="center"
            >
              <Button variant="contained">Postular</Button>
            </Stack>
          </Container>
        </Box>
        <Container sx={{ py: 6}} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {cards.map((card) => (
              <Grid item key={card} xs={12} sm={6} md={4}>
                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                >
                  
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Typography gutterBottom variant="h5" component="h2">
                      AREA DE TRABAJO
                    </Typography>
                    <BusinessCenterIcon sx={{ fontSize: 100 }} color="primary" />
                    <Typography>
                      Descripcion del trabajo
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small" href='vacante'>View</Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
      
    </ThemeProvider>

      <Footer/>
    </Box>

  )
}
