import React from 'react'
import { Container, Grid,Stack,Card,CardActions,CardContent,Box,Typography, createTheme,Button,ThemeProvider } from '@mui/material';


// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme();

export const Vacantes: React.FC <{}>=()=> {
  return (
    <ThemeProvider theme={defaultTheme}>
      
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: 'background.paper',
            pt: 4,
            pb: 6,
          }}
        >
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              Bolsa de empleos activa
            </Typography>
            
            <Stack
              sx={{ pt: 4 }}
              direction="row"
              spacing={2}
              justifyContent="center"
            >
              <Button variant="contained">Postular</Button>
            </Stack>
          </Container>
        </Box>
        <Container maxWidth="md">
          {/* End hero unit */}
          <Grid>
            
              
                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                >
                  
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Typography gutterBottom variant="h4" component="h2">
                      AREA DE TRABAJO
                    </Typography>
                    <Typography>
                      Descripcion del trabajo
                    </Typography>
                    <Typography gutterBottom variant="h5" component="h2">
                    Objetivo del Cargo
                    </Typography>
                    <Typography>
                      Descripcion del trabajo
                    </Typography>
                    <Typography gutterBottom variant="h5" component="h2">
                    Formación Requerida
                    </Typography>
                    <Typography>
                    Formación Específica
                    </Typography>
                    <Typography gutterBottom variant="h5" component="h2">
                    Formación Requerida
                    </Typography>
                    <Typography>
                    Experiencia Requerida
                    </Typography>
                    <Typography gutterBottom variant="h5" component="h2">
                    Formación Requerida
                    </Typography>
                    <Typography>
                      Experiencia Especifica
                    </Typography>
                    <Typography gutterBottom variant="h5" component="h2">
                    idiomas
                    </Typography>
                    <Typography>
                      Descripcion del trabajo
                    </Typography>
                    <Typography gutterBottom variant="h5" component="h2">
                    Herramientas de Software
                    </Typography>
                    <Typography>
                      Descripcion del trabajo
                    </Typography>
                    <Typography gutterBottom variant="h5" component="h2">
                    Conocimientos
                    </Typography>
                    <Typography>
                      Descripcion del trabajo
                    </Typography>
                    <Typography gutterBottom variant="h5" component="h2">
                    Habilidades/Destrezas
                    </Typography>
                    <Typography>
                      Descripcion del trabajo
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small">View</Button>
                  </CardActions>
                </Card>
              </Grid>
            
        </Container>
      </main>
      
    </ThemeProvider>
  );
}