import React from 'react'
import {
  Container,
  TextField,
  ThemeProvider,
  CssBaseline,
  Box, Tab,
  Typography,
  Grid, Button,
  Tabs, createTheme,
  FormControl, MenuItem,
  InputLabel, Select

} from '@mui/material';
import { styled } from '@mui/material/styles';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';


interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}
const VisuallyHiddenInput = styled('input')({
  clip: 'rect(0 0 0 0)',
  clipPath: 'inset(50%)',
  height: 1,
  overflow: 'hidden',
  position: 'absolute',
  bottom: 0,
  left: 0,
  whiteSpace: 'nowrap',
  width: 1,
});

function CustomTabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const defaultTheme = createTheme();

export const Postulacion: React.FC<{}> = () => {
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    console.log({
      cedula: data.get('cedula'),
      firstName: data.get('firstName'),
      lastName: data.get('lastName'),
      date: data.get('date'),
      pais: data.get('pais'),
      direccion: data.get('direccion'),
      email: data.get('email'),
      telefono: data.get('telefono'),


      cnt_hijos: data.get('cnt_hijos'),

      cargo: data.get('cargo'),
      // telefono: data.get('teleefono'),

    });

  };
  const [age, setAge] = React.useState('');
  const [value, setValue] = React.useState(0);
  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
    // setAge(event.target.value);
  };

  return (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >

          <Typography component="h1" variant="h5">
            Postule
          </Typography>
          <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              {/* <Grid item xs={12} sm={6}> */}

              <Grid item xs={9}>
                <TextField
                  required
                  fullWidth
                  id="ci"
                  label="Cedula"
                  name="ci"
                  autoComplete="ci"
                />

              </Grid>
              <Grid item xs={9}>
              <Button component="label" variant="contained" startIcon={<CloudUploadIcon />}>
                Subir Cedula
                <VisuallyHiddenInput type="file" />
              </Button>
              </Grid>
              <Grid item xs={9}>
              <Button component="label" variant="contained" startIcon={<CloudUploadIcon />}>
                Subir hoja de vida
                <VisuallyHiddenInput type="file" />
              </Button>
              </Grid>
              <Grid item xs={9}>
              <Button component="label" variant="contained" startIcon={<CloudUploadIcon />}>
                Subir 
                <VisuallyHiddenInput type="file" />
              </Button>
              </Grid>
             

              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="firstName"
                  label="Nombres"
                  type="firstName"
                  id="firstName"
                  autoComplete="new-password"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="lastName"
                  label="Apellidos"
                  type="lastName"
                  id="lastName"
                  autoComplete="lastName"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="date"
                  label=""
                  type="date"
                  id="date"
                  autoComplete="date" />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="pais"
                  label="Pais de nacimiento"
                  type="text"
                  id="pais"
                  autoComplete="pais" />
              </Grid>
              <Grid item xs={12}><Button component="label" variant="contained" startIcon={<CloudUploadIcon />}>
                Upload file
                <VisuallyHiddenInput type="file" />
              </Button>
                <TextField
                  required
                  fullWidth
                  name="dirccion"
                  label="Direccion"
                  type="text"
                  id="direccion"
                  autoComplete="direccion" />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="mail"
                  label="Ingrese su correo electronico"
                  type="email"
                  id="email"
                  autoComplete="email" />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="telefono"
                  label="Ingrese su numero Telefonico"
                  type="telefono"
                  id="telefono"
                  autoComplete="teleefono" />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  required
                  fullWidth
                  name="cnt_hijos"
                  label="Ingrese el numero de hijos que tiene"
                  type="number"
                  id="cntt_hijos"
                  autoComplete="cnt_hijos" />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="mail"
                  label="Ingrese su correo electronico"
                  type="email"
                  id="email"
                  autoComplete="email" />
              </Grid>
              <div>
                <FormControl sx={{ m: 1, minWidth: 80 }}>
                  <InputLabel id="cargo-label">cargo</InputLabel>
                  <Select
                    labelId="cargo-label"
                    id="cargo"
                    // value={age}
                    // onChange={handleChange}
                    // autoWidth
                    label="Edad"
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    <MenuItem value={10}>Tipo de cargo prueba</MenuItem>
                    <MenuItem value={21}>tipo de cargo</MenuItem>
                    <MenuItem value={22}>tipos d cargo </MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div>
                <FormControl sx={{ m: 1, minWidth: 80 }}>
                  <InputLabel id="demo-simple-select-autowidth-label">Genero</InputLabel>
                  <Select
                    labelId="demo-simple-select-autowidth-label"
                    id="demo-simple-select-autowidth"
                    // value={age}
                    // onChange={handleChange}
                    // autoWidth
                    label="Genero"
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    <MenuItem value={23}>Femenino</MenuItem>
                    <MenuItem value={24}>Masculino </MenuItem>
                  </Select>
                </FormControl>
              </div>


              <Grid item xs={12}>


              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Registrar
            </Button>
            <Grid container justifyContent="flex-end">

            </Grid>
          </Box>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
              <Tab label="Estudios" {...a11yProps(0)} />
              <Tab label="Idiomas" {...a11yProps(1)} />
              <Tab label="Experiencia" {...a11yProps(2)} />
              <Tab label="Cursos" {...a11yProps(3)} />
              <Tab label="Conocimientos" {...a11yProps(4)} />
              <Tab label="Herramientas" {...a11yProps(5)} />
              <Tab label="Habilidades" {...a11yProps(6)} />
              <Tab label="Refencias laborales" {...a11yProps(7)} />
            </Tabs>
          </Box>
          <CustomTabPanel value={value} index={0}>
            Ingrese sus estudios
          </CustomTabPanel>
          <CustomTabPanel value={value} index={1}>
            Ingrese sus idiomas
          </CustomTabPanel>
          <CustomTabPanel value={value} index={2}>
            Ingrese sus experiencias
          </CustomTabPanel>
          <CustomTabPanel value={value} index={3}>
            Ingrese sus cursos
          </CustomTabPanel>
          <CustomTabPanel value={value} index={4}>
            Ingrese sus conocimientos
          </CustomTabPanel>
          <CustomTabPanel value={value} index={5}>
            Ingrese sus herramientas
          </CustomTabPanel>
          <CustomTabPanel value={value} index={6}>
            Ingrese sus habilidades
          </CustomTabPanel>
          <CustomTabPanel value={value} index={7}>
            Ingrese sus Referencias laborales
          </CustomTabPanel>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
