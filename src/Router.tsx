import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { HomePage } from './view/home'
import { LoginPage } from './view/login'
import { RouterLayout } from './common/RouterLayout'
import { Register } from './view/register'
import { Vacantes } from './view/vacante'
import { Postulacion } from './view/vacante/postulacion'
import { Update } from './view/profile/update'
import { Profile } from './view/profile'

export const AppRouter: React.FC <{}>= () => {
  return (
    <Routes>
        <Route path='/' element={<RouterLayout/>}>
          
          <Route path='/vacante' element={<Vacantes/>}/>
          <Route path='/vacante/postulacion' element={<Postulacion/>}/>
          <Route path='/update' element={<Update/>}/>
        <Route path='/profile' element={<Profile/>}/>
        </Route>
        
        <Route path='/home' element={<HomePage/>}/>
        
        
        <Route path='/login' element={<LoginPage/>}/>
        <Route path='/register' element={<Register/>}/>
        
    </Routes>
  )
} 
